import {S3Cx} from "./lib/S3Cx";


export const FnDelete = async (bucketName: string) => {

  const sleep = (s: number) => {

    return new Promise(resolve => {
      setTimeout(resolve, s * 1000);
    })
  }

  const s3 = new S3Cx(bucketName, null);


  let isContinuar: boolean = true;

  let count:number=0;
  while (isContinuar) {

    try {

      const respuesta = await s3.listObjects();
      const listaProm = respuesta.Contents.map((k: any) => {
        return s3.removeKey(k.Key);
      })

      const ope = await Promise.all(listaProm);
      count++;

      const msg=`${bucketName} - ${count}`;

      console.log(msg)

      isContinuar = respuesta.IsTruncated;

    } catch (error) {

      console.log(error);
      await sleep(3);

    }

  }

  console.log('terminado');

}