import {AWSError} from "aws-sdk";
import {DeleteObjectOutput} from "aws-sdk/clients/s3";
import * as stream from "stream";


const AWS: any = require('aws-sdk');


/***
 * Adminstrador del Bucker privado para archivos de procesamiento del sistema.
 *
 */
export class S3Cx {
  protected Bucket: string
  protected s3: any;

  constructor(AWSBucketName: string, profileCredencials: string | null) {

    this.Bucket = AWSBucketName;
    if (profileCredencials) {
      AWS.config.credentials = new AWS.SharedIniFileCredentials({profile: profileCredencials});
    }
    this.s3 = new AWS.S3({apiVersion: '2006-03-01'});
  }

  async getUrlVer(keyS3: string, expiresMin: number = 60) {

    const s3 = this.s3;

    const s3Params: any = {
      Bucket: this.Bucket,
      Key: keyS3,
      Expires: expiresMin * 60
    };

    return await s3.getSignedUrlPromise('getObject', s3Params);

  }


  async getUrlPostFromForm(keyS3: string, ContentType: string, isDownloadResourse: boolean = false, expiresSeconds: number = 60 * 60): Promise<string> {
    const s3 = this.s3;

    const s3Params: any = {
      Bucket: this.Bucket,
      Fields: {
        key: keyS3,
      },
      Expires: expiresSeconds,
    };


    s3Params.Fields["Content-Type"] = ContentType;
    if (isDownloadResourse) {
      s3Params.Fields["Content-Disposition"] = "attachment";
    } else {
      s3Params.Fields["Content-Disposition"] = "inline";
    }


    const promise: Promise<string> = new Promise((resolve, reject) => {

      s3.createPresignedPost(s3Params, function (err: AWSError, data: string) {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });

    return promise;

  }


  async getUrlPut(keyS3: string, contentType?: string, expiresSeconds: number = 10000): Promise<string> {

    const s3 = this.s3;

    const s3Params: any = {
      Bucket: this.Bucket,
      Key: keyS3,
      Expires: expiresSeconds,
    };

    if (contentType) {
      s3Params.ContentType = contentType
    }

    return await s3.getSignedUrlPromise('putObject', s3Params);

  }

  async removeKey(keyS3: string) {
    const s3 = this.s3;
    const s3Params = {
      Bucket: this.Bucket,
      Key: keyS3
    };


    const promise = new Promise((resolve, reject) => {

      s3.deleteObject(s3Params, function (err: AWSError, data: DeleteObjectOutput) {
        if (err) {
          reject(err);
        } else {
          resolve(true);
        }
      });
    });

    return promise;
  }

  async getObjectTxt(Key: string, encode = 'utf-8'): Promise<string> {

    const s3 = this.s3;
    const s3Params = {
      Bucket: this.Bucket,
      Key
    };


    const promise = new Promise((resolve, reject) => {

      s3.getObject(s3Params, function (err: AWSError, data: any) {
        if (err) {
          reject(err);
        } else {
          const texto = data.Body.toString(encode);
          resolve(texto);
        }
      });

    });

    const respuesta: any = await promise;

    return respuesta.toString();

  }


  async getObject(Key: string): Promise<any> {
    const s3 = this.s3;
    const s3Params = {
      Bucket: this.Bucket,
      Key
    };


    return new Promise((resolve, reject) => {

      s3.getObject(s3Params, function (err: AWSError, data: any) {
        if (err) {
          reject(err);
        } else {
          resolve(data.Body);
        }
      });

    });


  }

  /***
   * Guardar un json saveObject(object)
   * @param Key
   * @param ObjectToSaveAsJson
   * @returns {Promise<boolean>}
   */
  async saveObject(Key: string, ObjectToSaveAsJson: any): Promise<any> {
    const s3 = this.s3;
    const s3Params = {
      Bucket: this.Bucket,
      Key,
      Body: JSON.stringify(ObjectToSaveAsJson)
    };

    const promise = new Promise((resolve, reject) => {

      s3.putObject(s3Params, function (err: AWSError, data: any) {
        if (err) {
          reject(err);
        } else {
          resolve(!!data.ETag);
        }
      });

    });

    const respuesta = await promise;

    return respuesta;
  }

  /***
   *
   * @param {string} Key
   * @param {string} texto
   * @returns {Promise<void>}
   */
  async saveTxt(Key: string, texto: string): Promise<any> {
    const s3 = this.s3;
    const s3Params = {
      Bucket: this.Bucket,
      Key,
      Body: texto,
      ContentEncoding: "text/plain"
    };

    const promise = new Promise((resolve, reject) => {

      s3.putObject(s3Params, function (err: AWSError, data: any) {
        if (err) {
          reject(err);
        } else {
          resolve(!!data.ETag);
        }
      });

    });

    return await promise;
  }

  async saveZip(Key: string, readStream: stream): Promise<any> {

    const s3 = this.s3;
    const s3Params = {
      Bucket: this.Bucket,
      Key,
      Body: readStream
    };

    const promise = new Promise((resolve, reject) => {

      s3.putObject(s3Params, function (err: AWSError, data: any) {
        if (err) {
          reject(err);
        } else {
          resolve(!!data.ETag);
        }
      });

    });

    return await promise;
  }

  async saveImg64(Key: string, img64: string, tipoMimeImg: string): Promise<void> {
    const s3 = this.s3;
    const buf: any = Buffer.from(img64.replace(/^data:image\/\w+;base64,/, ""), 'base64')

    const s3Params = {
      Bucket: this.Bucket,
      Key,
      Body: buf,
      ContentEncoding: 'base64',
      ContentType: tipoMimeImg

    };

    const promise = new Promise((resolve, reject) => {

      s3.putObject(s3Params, function (err: AWSError, data: any) {
        if (err) {
          reject(err);
        } else {
          resolve(!!data.ETag);
        }
      });

    });

    const respuesta = await promise;

  }


  async listObjects(partialKey: string = ''): Promise<any> {
    const s3 = this.s3;
    const s3Params: any = {
      Bucket: this.Bucket
    };

    if (partialKey) {
      s3Params.Prefix = partialKey
    }


    return new Promise((resolve, reject) => {

      s3.listObjectsV2(s3Params, function (err: AWSError, data: any) {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });

    });
  }

}


