"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.S3Cx = void 0;
const AWS = require('aws-sdk');
/***
 * Adminstrador del Bucker privado para archivos de procesamiento del sistema.
 *
 */
class S3Cx {
    constructor(AWSBucketName, profileCredencials) {
        this.Bucket = AWSBucketName;
        if (profileCredencials) {
            AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: profileCredencials });
        }
        this.s3 = new AWS.S3({ apiVersion: '2006-03-01' });
    }
    async getUrlVer(keyS3, expiresMin = 60) {
        const s3 = this.s3;
        const s3Params = {
            Bucket: this.Bucket,
            Key: keyS3,
            Expires: expiresMin * 60
        };
        return await s3.getSignedUrlPromise('getObject', s3Params);
    }
    async getUrlPostFromForm(keyS3, ContentType, isDownloadResourse = false, expiresSeconds = 60 * 60) {
        const s3 = this.s3;
        const s3Params = {
            Bucket: this.Bucket,
            Fields: {
                key: keyS3,
            },
            Expires: expiresSeconds,
        };
        s3Params.Fields["Content-Type"] = ContentType;
        if (isDownloadResourse) {
            s3Params.Fields["Content-Disposition"] = "attachment";
        }
        else {
            s3Params.Fields["Content-Disposition"] = "inline";
        }
        const promise = new Promise((resolve, reject) => {
            s3.createPresignedPost(s3Params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data);
                }
            });
        });
        return promise;
    }
    async getUrlPut(keyS3, contentType, expiresSeconds = 10000) {
        const s3 = this.s3;
        const s3Params = {
            Bucket: this.Bucket,
            Key: keyS3,
            Expires: expiresSeconds,
        };
        if (contentType) {
            s3Params.ContentType = contentType;
        }
        return await s3.getSignedUrlPromise('putObject', s3Params);
    }
    async removeKey(keyS3) {
        const s3 = this.s3;
        const s3Params = {
            Bucket: this.Bucket,
            Key: keyS3
        };
        const promise = new Promise((resolve, reject) => {
            s3.deleteObject(s3Params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(true);
                }
            });
        });
        return promise;
    }
    async getObjectTxt(Key, encode = 'utf-8') {
        const s3 = this.s3;
        const s3Params = {
            Bucket: this.Bucket,
            Key
        };
        const promise = new Promise((resolve, reject) => {
            s3.getObject(s3Params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    const texto = data.Body.toString(encode);
                    resolve(texto);
                }
            });
        });
        const respuesta = await promise;
        return respuesta.toString();
    }
    async getObject(Key) {
        const s3 = this.s3;
        const s3Params = {
            Bucket: this.Bucket,
            Key
        };
        return new Promise((resolve, reject) => {
            s3.getObject(s3Params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data.Body);
                }
            });
        });
    }
    /***
     * Guardar un json saveObject(object)
     * @param Key
     * @param ObjectToSaveAsJson
     * @returns {Promise<boolean>}
     */
    async saveObject(Key, ObjectToSaveAsJson) {
        const s3 = this.s3;
        const s3Params = {
            Bucket: this.Bucket,
            Key,
            Body: JSON.stringify(ObjectToSaveAsJson)
        };
        const promise = new Promise((resolve, reject) => {
            s3.putObject(s3Params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(!!data.ETag);
                }
            });
        });
        const respuesta = await promise;
        return respuesta;
    }
    /***
     *
     * @param {string} Key
     * @param {string} texto
     * @returns {Promise<void>}
     */
    async saveTxt(Key, texto) {
        const s3 = this.s3;
        const s3Params = {
            Bucket: this.Bucket,
            Key,
            Body: texto,
            ContentEncoding: "text/plain"
        };
        const promise = new Promise((resolve, reject) => {
            s3.putObject(s3Params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(!!data.ETag);
                }
            });
        });
        return await promise;
    }
    async saveZip(Key, readStream) {
        const s3 = this.s3;
        const s3Params = {
            Bucket: this.Bucket,
            Key,
            Body: readStream
        };
        const promise = new Promise((resolve, reject) => {
            s3.putObject(s3Params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(!!data.ETag);
                }
            });
        });
        return await promise;
    }
    async saveImg64(Key, img64, tipoMimeImg) {
        const s3 = this.s3;
        const buf = Buffer.from(img64.replace(/^data:image\/\w+;base64,/, ""), 'base64');
        const s3Params = {
            Bucket: this.Bucket,
            Key,
            Body: buf,
            ContentEncoding: 'base64',
            ContentType: tipoMimeImg
        };
        const promise = new Promise((resolve, reject) => {
            s3.putObject(s3Params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(!!data.ETag);
                }
            });
        });
        const respuesta = await promise;
    }
    async listObjects(partialKey = '') {
        const s3 = this.s3;
        const s3Params = {
            Bucket: this.Bucket
        };
        if (partialKey) {
            s3Params.Prefix = partialKey;
        }
        return new Promise((resolve, reject) => {
            s3.listObjectsV2(s3Params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data);
                }
            });
        });
    }
}
exports.S3Cx = S3Cx;
//# sourceMappingURL=S3Cx.js.map