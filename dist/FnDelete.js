"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FnDelete = void 0;
const S3Cx_1 = require("./lib/S3Cx");
const FnDelete = async (bucketName) => {
    const sleep = (s) => {
        return new Promise(resolve => {
            setTimeout(resolve, s * 1000);
        });
    };
    const s3 = new S3Cx_1.S3Cx(bucketName, null);
    let isContinuar = true;
    let count = 0;
    while (isContinuar) {
        try {
            const respuesta = await s3.listObjects();
            const listaProm = respuesta.Contents.map((k) => {
                return s3.removeKey(k.Key);
            });
            const ope = await Promise.all(listaProm);
            count++;
            const msg = `${bucketName} - ${count}`;
            console.log(msg);
            isContinuar = respuesta.IsTruncated;
        }
        catch (error) {
            console.log(error);
            await sleep(3);
        }
    }
    console.log('terminado');
};
exports.FnDelete = FnDelete;
//# sourceMappingURL=FnDelete.js.map